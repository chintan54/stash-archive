package com.atlassian.bitbucket.internal.archive.command;

import com.atlassian.bitbucket.archive.ArchiveFormat;
import com.atlassian.bitbucket.io.TypeAwareOutputSupplier;
import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.utils.process.BaseOutputHandler;
import com.atlassian.utils.process.ProcessException;
import com.google.common.io.ByteStreams;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ArchiveOutputHandler extends BaseOutputHandler implements CommandOutputHandler<Void> {

    private final String contentType;
    private final TypeAwareOutputSupplier outputSupplier;

    public ArchiveOutputHandler(ArchiveFormat format, TypeAwareOutputSupplier outputSupplier) {
        this.contentType = contentTypeFor(format);
        this.outputSupplier = outputSupplier;
    }

    @Override
    public Void getOutput() {
        return null;
    }

    @Override
    public void process(InputStream stdOut) throws ProcessException {
        try (BufferedInputStream inputStream = new BufferedInputStream(stdOut, 4096)) { //4k to match ByteStreams
            inputStream.mark(2);

            int read = inputStream.read();
            if (read == -1) {
                //If there's no data on the input stream, it most likely means the command has
                //failed (probably due to an invalid commit). To try and ensure a well-formed
                //error is sent to the client, if we're streaming over REST, we don't want to
                //request the output stream from the supplier
                return;
            } else {
                //Otherwise, if there's any data available on the input stream, stream it
                inputStream.reset();
            }

            try (OutputStream outputStream = outputSupplier.getStream(contentType)) {
                ByteStreams.copy(inputStream, outputStream);
            }
        } catch (IOException e) {
            throw new ProcessException(e);
        }
    }

    private static String contentTypeFor(ArchiveFormat format) {
        if (format == ArchiveFormat.TAR) {
            return "application/x-tar";
        }
        return "application/octet-stream";
    }
}