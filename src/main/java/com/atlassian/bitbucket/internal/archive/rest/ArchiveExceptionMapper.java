package com.atlassian.bitbucket.internal.archive.rest;

import com.atlassian.bitbucket.rest.exception.UnhandledExceptionMapper;
import com.atlassian.bitbucket.rest.exception.UnhandledExceptionMapperHelper;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.ext.Provider;

/**
 * @since 2.1
 */
@Provider
@Singleton
public class ArchiveExceptionMapper extends UnhandledExceptionMapper {

    public ArchiveExceptionMapper(UnhandledExceptionMapperHelper helper) {
        super(helper);
    }
}
