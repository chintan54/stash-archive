package it.com.atlassian.bitbucket.internal.archive.rest;

import com.atlassian.bitbucket.archive.ArchiveFormat;
import com.atlassian.bitbucket.commit.NoSuchCommitException;
import com.atlassian.bitbucket.test.BaseRetryingFuncTest;
import com.atlassian.bitbucket.test.DefaultFuncTestData;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import static com.atlassian.bitbucket.test.DefaultFuncTestData.*;
import static java.util.stream.Collectors.joining;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Verifies archive contents, downloading archives from both the REST endpoint introduced in 2.1 and the servlet
 * which has been present since the initial version.
 * <p>
 * All of the various query parameters which are supported by the REST endpoint are tested against both the REST
 * endpoint and the servlet, to ensure they work unchanged in both places.
 */
@RunWith(Parameterized.class)
@SuppressWarnings("unchecked")
public class ArchiveResourceTest extends BaseRetryingFuncTest {

    private static final Pattern PATTERN_DISPOSITION = Pattern.compile("attachment; filename=\"([^\"]+)\"");

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    private final Supplier<StringBuilder> urlSupplier;

    public ArchiveResourceTest(Supplier<StringBuilder> urlSupplier) {
        this.urlSupplier = urlSupplier;
    }

    @Parameterized.Parameters
    public static Object[] data() {
        return new Object[] {
                new RestUrlSupplier(),
                new ServletUrlSupplier()
        };
    }

    @Test
    public void testGetArchiveForTar() throws IOException {
        File tar = getArchive(null, "default-repository.tar", ArchiveFormat.TAR, null, "a path with spaces/");
        assertEquals("default-repository.tar", tar.getName());

        List<String> files = listArchive(new TarArchiveInputStream(new FileInputStream(tar)));
        assertThat(files, contains(
                equalTo("a path with spaces/add_file/add_file.txt"),
                equalTo("a path with spaces/basic_branching/file.txt"),
                equalTo("a path with spaces/binary/B.zip"),
                equalTo("a path with spaces/binary/C.zip"),
                equalTo("a path with spaces/binary/D.zip"),
                equalTo("a path with spaces/branch_mod_merge/file.txt"),
                equalTo("a path with spaces/modification/mod_file.txt"),
                equalTo("a path with spaces/moved_dir/file.txt"),
                equalTo("a path with spaces/mv_file/moved_file.txt"),
                equalTo("a path with spaces/out_of_order/file.txt"),
                equalTo("a path with spaces/symlink/link.txt"),
                equalTo("a path with spaces/symlink/target.txt")));
    }

    @Test
    public void testGetArchiveForTarGz() throws IOException {
        for (String extension : ArchiveFormat.TAR_GZ.getExtensions()) {
            File tgz = getArchive("bf3346620ec9161977fc23dee1240a7991e64ca1", null, extension, null, null);
            //When there's no ref, the default filename should be "slug-commitId.format"
            assertEquals(getProject1Repository1() + "-bf3346620ec9161977fc23dee1240a7991e64ca1.tar.gz", tgz.getName());

            List<String> files = listArchive(new TarArchiveInputStream(new GZIPInputStream(new FileInputStream(tgz))));
            assertThat(files, contains(equalTo("modification/mod_file.txt"), equalTo("out_of_order/file.txt")));
        }
    }

    @Test
    public void testGetArchiveForZip() throws IOException {
        for (String extension : Iterables.concat(ArchiveFormat.ZIP.getExtensions(), Collections.singleton(null))) {
            File zip = getArchive("master", null, extension, ImmutableSet.of("add_file", "mv_file", "symlink"), null);
            assertEquals(getProject1Repository1() + "-master@0a943a29376.zip", zip.getName());

            List<String> files = listArchive(new ZipArchiveInputStream(new FileInputStream(zip)));
            assertThat(files, contains(
                    equalTo("add_file/add_file.txt"),
                    equalTo("mv_file/moved_file.txt"),
                    equalTo("symlink/link.txt"),
                    equalTo("symlink/target.txt")));
        }
    }

    @Test
    public void testArchiveWithNonexistentCommit() {
        RestAssured.given()
                .auth().preemptive().basic(getAdminUser(), getAdminPassword())
                .expect().statusCode(NOT_FOUND.getStatusCode())
                .log().ifStatusCodeMatches(not(equalTo(NOT_FOUND.getStatusCode())))
                .body("errors[0].exceptionName", equalTo(NoSuchCommitException.class.getCanonicalName()))
                .body("errors[0].message", containsString("Commit 'c6e2cfef4adcdf901eae1ba9db266991cbcc2be3' does not exist"))
                .when().get(urlSupplier.get().append("?at=c6e2cfef4adcdf901eae1ba9db266991cbcc2be3").toString());
    }

    private File getArchive(String commitId, String filename, ArchiveFormat format,
                            Set<String> paths, String prefix) throws IOException {
        return getArchive(commitId, filename, Iterables.getOnlyElement(format.getExtensions()), paths, prefix);
    }

    private File getArchive(String commitId, String filename, String format, Set<String> paths, String prefix)
            throws IOException {
        StringBuilder builder = urlSupplier.get();

        String separator = "?";
        if (commitId != null) {
            builder.append(separator).append("at=").append(commitId);
            separator = "&";
        }
        if (filename != null) {
            builder.append(separator).append("filename=").append(filename);
            separator = "&";
        }
        if (format != null) {
            builder.append(separator).append("format=").append(format);
            separator = "&";
        }
        if (paths != null) {
            builder.append(separator).append("path=")
                    .append(paths.stream().collect(joining("&path=")));
            separator = "&";
        }
        if (prefix != null) {
            builder.append(separator).append("prefix=").append(prefix);
        }

        Response response = RestAssured.given()
                .auth().preemptive().basic(getAdminUser(), getAdminPassword())
                .expect().statusCode(OK.getStatusCode())
                .log().ifError()
                .when().get(builder.toString());

        try (InputStream inputStream = response.asInputStream()) {
            Matcher matcher = PATTERN_DISPOSITION.matcher(response.getHeader("Content-Disposition"));
            String name = matcher.matches() ? matcher.group(1) : filename == null ? "archive.dat" : filename;

            File archive = new File(folder.getRoot(), name);
            Files.copy(inputStream, archive.toPath(), StandardCopyOption.REPLACE_EXISTING);

            return archive;
        }

    }

    @SuppressWarnings("Duplicates") //Also in DefaultGitExtendedCommandFactoryTest
    private static List<String> listArchive(ArchiveInputStream s) throws IOException {
        List<String> files = new ArrayList<>();
        try (ArchiveInputStream inputStream = s) {
            ArchiveEntry entry;
            while ((entry = inputStream.getNextEntry()) != null) {
                if (entry.isDirectory() || "pax_global_header".equals(entry.getName())) {
                    //For simplicity, we only look at the files in the archive. Additionally, commons-compress
                    //isn't PAX-aware so the pax_global_header added by Git (listing the archived commit) gets
                    //included as a file entry and needs to be ignored.
                    continue;
                }
                files.add(entry.getName());
            }
        }

        return files;
    }

    /**
     * Verifies archives can be downloaded from the REST resource.
     */
    private static class RestUrlSupplier implements Supplier<StringBuilder> {

        @Override
        public StringBuilder get() {
            return new StringBuilder(DefaultFuncTestData.getRestURL("archive", "latest"))
                    .append(getRepositoryPath(getProject1(), getProject1Repository1()))
                    .append("/archive");
        }
    }

    /**
     * Verifies archives can still be downloaded using the legacy servlet URL from previous versions.
     */
    private static class ServletUrlSupplier implements Supplier<StringBuilder> {

        @Override
        public StringBuilder get() {
            return new StringBuilder(DefaultFuncTestData.getBaseURL())
                    .append("/plugins/servlet/archive/projects/")
                    .append(DefaultFuncTestData.getProject1())
                    .append("/repos/")
                    .append(DefaultFuncTestData.getProject1Repository1());
        }
    }
}
